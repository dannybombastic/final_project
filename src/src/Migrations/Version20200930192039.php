<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200930192039 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE vehicle (id UUID NOT NULL, type VARCHAR(255) NOT NULL, fuel VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN vehicle.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE article (id UUID NOT NULL, owner_id UUID NOT NULL, vehicles_id UUID NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_published BOOLEAN NOT NULL, price NUMERIC(7, 2) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_23A0E667E3C61F9 ON article (owner_id)');
        $this->addSql('CREATE INDEX IDX_23A0E6616F10C70 ON article (vehicles_id)');
        $this->addSql('COMMENT ON COLUMN article.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN article.owner_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN article.vehicles_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE valuation (id INT NOT NULL, owner_id UUID NOT NULL, article_id UUID NOT NULL, stars INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_216E16997E3C61F9 ON valuation (owner_id)');
        $this->addSql('CREATE INDEX IDX_216E16997294869C ON valuation (article_id)');
        $this->addSql('COMMENT ON COLUMN valuation.owner_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN valuation.article_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE "user" (id UUID NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, phone_number VARCHAR(255) DEFAULT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, allowed JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('COMMENT ON COLUMN "user".id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE messages (id INT NOT NULL, target_user_id UUID DEFAULT NULL, subject VARCHAR(255) NOT NULL, message TEXT NOT NULL, is_read BOOLEAN NOT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DB021E966C066AFE ON messages (target_user_id)');
        $this->addSql('COMMENT ON COLUMN messages.target_user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE comment (id UUID NOT NULL, owner_id UUID NOT NULL, article_id UUID NOT NULL, commentary VARCHAR(255) NOT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9474526C7E3C61F9 ON comment (owner_id)');
        $this->addSql('CREATE INDEX IDX_9474526C7294869C ON comment (article_id)');
        $this->addSql('COMMENT ON COLUMN comment.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN comment.owner_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN comment.article_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE media_object (id INT NOT NULL, article_id UUID DEFAULT NULL, file_path VARCHAR(255) DEFAULT NULL, position INT DEFAULT NULL, original_filename VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_14D431327294869C ON media_object (article_id)');
        $this->addSql('COMMENT ON COLUMN media_object.article_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E667E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E6616F10C70 FOREIGN KEY (vehicles_id) REFERENCES vehicle (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valuation ADD CONSTRAINT FK_216E16997E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valuation ADD CONSTRAINT FK_216E16997294869C FOREIGN KEY (article_id) REFERENCES article (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE messages ADD CONSTRAINT FK_DB021E966C066AFE FOREIGN KEY (target_user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C7294869C FOREIGN KEY (article_id) REFERENCES article (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE media_object ADD CONSTRAINT FK_14D431327294869C FOREIGN KEY (article_id) REFERENCES article (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

      
        $this->addSql('ALTER TABLE article DROP CONSTRAINT FK_23A0E6616F10C70');
        $this->addSql('ALTER TABLE valuation DROP CONSTRAINT FK_216E16997294869C');
        $this->addSql('ALTER TABLE comment DROP CONSTRAINT FK_9474526C7294869C');
        $this->addSql('ALTER TABLE media_object DROP CONSTRAINT FK_14D431327294869C');
        $this->addSql('ALTER TABLE article DROP CONSTRAINT FK_23A0E667E3C61F9');
        $this->addSql('ALTER TABLE valuation DROP CONSTRAINT FK_216E16997E3C61F9');
        $this->addSql('ALTER TABLE messages DROP CONSTRAINT FK_DB021E966C066AFE');
        $this->addSql('ALTER TABLE comment DROP CONSTRAINT FK_9474526C7E3C61F9');
        $this->addSql('DROP TABLE vehicle');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE valuation');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE messages');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE media_object');
    }
}
