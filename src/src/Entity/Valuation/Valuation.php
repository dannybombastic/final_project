<?php

namespace App\Entity\Valuation;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\User;
use App\Repository\ValuationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
 

/**
 * @ApiResource(
 * security="is_granted('ROLE_USER')",
 * collectionOperations={
 *          "get"={
 *             "normalization_context"={"groups"={"valuation:read", "valuation:item:get"}}
 *          },
 *           "post"
 *         },
 *     itemOperations={
 *          "get",
 *          "put",
 *         "delete",
 *     }, 
 *     normalizationContext={"groups"={"valuation:read"}, "swagger_definition_name"="Read"},
 *     denormalizationContext={"groups"={"valuation:write"}, "swagger_definition_name"="Write"},
 * )
 * @ORM\Entity(repositoryClass=ValuationRepository::class)
 */
class Valuation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="valuations")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"valuation:write", "valuation:item:get"})
     * @Assert\NotBlank()
     */
    private $owner;


    /**
     * @ORM\Column(type="integer")
     * @Groups({"valuation:write", "valuation:read"})
     * @Assert\Positive()
     */
    private $stars;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

 
   

    public function getStars(): ?int
    {
        return $this->stars;
    }

    public function setStars(int $stars): self
    {
        $this->stars = $stars;

        return $this;
    }
}
