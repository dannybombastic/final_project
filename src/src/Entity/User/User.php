<?php

namespace App\Entity\User;

use App\Entity\Article\Article;
use App\Entity\Comment\Comment;
use App\Entity\Message\Messages;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Valuation\Valuation;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Ramsey\Uuid\Uuid;
use Doctrine\ORM\Id\UuidGenerator;
/**
 * 
 * @ApiResource(
 *   security="is_granted('ROLE_USER')",
 *      collectionOperations={
 *          "get",
 *          "post"= {
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *              "validation_groups" = {"Default", "create"}
 *          },          
 *      },
 *      itemOperations={
 *          "get"={
 *                  "normalization_context"={"groups"={"user:read", "user:item:get"}}
 *          },
 *          "put"= {
 *             "denormalization_context"={"groups"={"user:write", "user:item:put"}},  
 *             "security"="is_granted('ROLE_USER') and object == user"},
 *             "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *          }, 
 * normalizationContext={"groups"="user:read", "swagger_definition_name"="Read"},
 * denormalizationContext={"groups"="user:write", "swagger_definition_name"="Write"},
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"username"}, message="the user name must be unique")
 * @UniqueEntity(fields={"email"}, message="the user name must be unique")
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
     /**
     * @var \Ramsey\Uuid\UuidInterface
     * @ORM\Column(name="id", type="uuid", length=40, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"user:write", "user:read", "user:item:put", "article:item:get"})
     */

    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user:read", "user:write", "article:item:get"})
     * @Assert\NotBlank(groups={"create"}, message="the email should no be balnk")
     * @Assert\Email(groups={"create"}, message="the email have a weird format try again")
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     *  
     */
    private $password;

    /**
     * @Groups({"user:write"})
     * @Assert\NotBlank(groups={"create"})
     * @SerializedName("password")
     */
    private $plainPassword;


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Groups({"user:write", "user:read", "article:item:get"})
     * 
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user:write", "user:read", "article:item:get"})
     * @Assert\NotBlank(groups={"create"})
     * @Assert\Positive(groups={"create"})
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTime $createAt;

    /**
     * @ORM\OneToMany(targetEntity=Messages::class, mappedBy="targetUser")
     * @Groups({"user:item:get"})
     * 
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity=Article::class, mappedBy="owner", cascade={"persist"}, orphanRemoval=true)
     * @Groups({"user:item:get"})
     */
    private $articles;

    /**
     * @ORM\Column(type="json", nullable=true)
     *  
     * @Groups({"user:item:put"})
     */
    private $allowed = [];

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="owner", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=Valuation::class, mappedBy="owner", orphanRemoval=true)
     */
    private $valuations;


    public function __construct(string $id = null)
    {
        $this->id = $id ?? Uuid::uuid4()->toString();
        $this->createAt = new \DateTime();
        $this->messages = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->valuations = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
         $this->plainPassword = null;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getCreateAt(): ?DateTime
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTime $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get the value of plainPassword
     */ 
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * Set the value of plainPassword
     *
     * @return  self
     */ 
    public function setPlainPassword(string $plainPassword) : self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @return Collection|Messages[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Messages $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setOwner($this);
        }

        return $this;
    }

    public function removeMessage(Messages $message): self
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
            // set the owning side to null (unless already changed)
            if ($message->getOwner() === $this) {
                $message->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setOwner($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            // set the owning side to null (unless already changed)
            if ($article->getOwner() === $this) {
                $article->setOwner(null);
            }
        }

        return $this;
    }

    public function getAllowed(): array
    {
        return  array_unique($this->allowed);
    }

    public function setAllowed(array $allowed): self
    {
        $this->allowed = $allowed;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setOwner($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getOwner() === $this) {
                $comment->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Valuation[]
     */
    public function getValuations(): Collection
    {
        return $this->valuations;
    }

    public function addValuation(Valuation $valuation): self
    {
        if (!$this->valuations->contains($valuation)) {
            $this->valuations[] = $valuation;
            $valuation->setOwner($this);
        }

        return $this;
    }

    public function removeValuation(Valuation $valuation): self
    {
        if ($this->valuations->contains($valuation)) {
            $this->valuations->removeElement($valuation);
            // set the owning side to null (unless already changed)
            if ($valuation->getOwner() === $this) {
                $valuation->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
