<?php

namespace App\Entity\MediaObject;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Article\Article;
use App\Repository\MediaObjectRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\Uploader\UploaderAction;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;


/**
 * @ORM\Entity
 * @ApiResource(
 *   attributes={"order"={"position": "ASC"}},
 *  formats={"json"},
 *  security="is_granted('ROLE_USER')",
 *  iri="http://schema.org/MediaObject",
 *  normalizationContext={"groups"={"media:read"}, "swagger_definition_name"="Read"},
 *  denormalizationContext={"groups"={"media:write"}, "swagger_definition_name"="Write"},
 *     collectionOperations={
 *         "post"={
 *             "controller"=UploaderAction::class,
 *             "deserialize"=false,
 *             "validation_groups"={"Default", "media:create"},
 *             "openapi_context"={
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *         "get"={
 *           "normalization_context"={"groups"={"media:read", "media:item:get"}},
 *          }     
 *     },
 *     itemOperations={
 *         "get"={
 *           "normalization_context"={"groups"={"media:read", "media:item:get"}},
 *          },
 *         "put"={
 *              "security_post_denormalize"="is_granted('EDIT_MEDIA', object)",
 *              "controller"=UploaderAction::class,
 *              "validation_groups"={"Default", "media:update"},
 *              "denormalization_context"={"groups"={"media:write", "media:item:put"}},
 *       },
 *      "delete"={
 *             "security_post_denormalize"="is_granted('EDIT_MEDIA', object)",
 *          }
 *     },
 * )
 * @ApiFilter(SearchFilter::class, properties={
 *     "article": "exact", 
 *     "article.owner": "exact"    
 * })
 *  
 * 
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass=MediaObjectRepository::class)
 * 
 */
class MediaObject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"media:read", "user:read"})
     */
    private $id;


    /**
     * @var string|null
     *
     * @ApiProperty(iri="http://schema.org/contentUrl")
     * @Groups({"media:read", "article:read", "user:read"})
     */
    public $contentUrl;

    /**
     * @var File|null
     *@Assert\File(
    *     maxSize = "2048k",
    *     maxSizeMessage = "File exceeds allowed size",
    *     mimeTypes = {"image/png","image/jpeg", "application/pdf", "application/x-pdf"},
    *     mimeTypesMessage = "Please upload a valid file"
    * )
     * @Assert\NotNull(groups={"media:create"})
     * @Vich\UploadableField(mapping="media_object", fileNameProperty="filePath")
     * 
     */
    public $file;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    public $filePath;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="mediaObjects")
     * @Groups({"media:item:get", "media:item:put", "media:read"})
     * @Assert\NotBlank(groups="media:update")
     *
     */
    private $article;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"media:read", "media:write"})
     * 
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"media:read","media:write"})
     */
    private $originalFilename;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getOriginalFilename(): ?string
    {
        return $this->originalFilename;
    }

    public function setOriginalFilename(?string $originalFilename): self
    {
        $this->originalFilename = $originalFilename ?? $this->getFilePath();;

        return $this;
    }

    /**
     * Get the value of contentUrl
     *
     * @return  string|null
     *  
     */ 
    
    public function getContentUrl()
    {
        return $this->contentUrl;
    }

    /**
     * Set the value of contentUrl
     *
     * @param  string|null  $contentUrl
     *
     * @return  self
     */ 
   
    /**
     * Get the value of filePath
     *
     * @return  string|null
     */ 
    public function getFilePath()
    {
        return $this->filePath;
    }

   
}
