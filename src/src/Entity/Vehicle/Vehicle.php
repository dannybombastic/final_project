<?php

namespace App\Entity\Vehicle;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Article\Article;
use App\Repository\VehiclesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Ramsey\Uuid\Uuid;
use Doctrine\ORM\Id\UuidGenerator;

/**
 * @ApiResource(
 *  security="is_granted('ROLE_USER')",
 * normalizationContext={"groups"={"vehicles:read"}, "swagger_definition_name"="Read"},
 * denormalizationContext={"groups"={"vehicles:write"}, "swagger_definition_name"="Write"},
 * 
 * 
 * )
 * @ORM\Entity(repositoryClass=VehiclesRepository::class)
 */
class Vehicle
{
     /**
     * @var \Ramsey\Uuid\UuidInterface
     * @ORM\Column(name="id", type="uuid", length=40, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"vehicles:read", "vehicles:write", "article:read"})
     */

    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"vehicles:read", "vehicles:write", "user:read"})
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"vehicles:read", "vehicles:write"})
     * @Assert\NotBlank()
     */
    private $fuel;

    /**
     * @ORM\OneToMany(targetEntity=Article::class, mappedBy="vehicles")
     * @Groups({"vehicles:read"})
     * @Assert\NotBlank()
     */
    private $article;

    public function __construct(string $id = null)
    {
        $this->id = $id ?? Uuid::uuid4()->toString();
        $this->article = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getFuel(): ?string
    {
        return $this->fuel;
    }

    public function setFuel(string $fuel): self
    {
        $this->fuel = $fuel;

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticle(): Collection
    {
        return $this->article;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->article->contains($article)) {
            $this->article[] = $article;
            $article->setVehicles($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->article->contains($article)) {
            $this->article->removeElement($article);
            // set the owning side to null (unless already changed)
            if ($article->getVehicles() === $this) {
                $article->setVehicles(null);
            }
        }

        return $this;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
