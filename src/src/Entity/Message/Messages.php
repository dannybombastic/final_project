<?php

namespace App\Entity\Message;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\User;
use App\Repository\MessagesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *  collectionOperations={
 *          "get"={
 *              "security_post_normalize"="is_granted('EDIT_ARTICLE', object)",
 *              "security_message"="only the creator can edit message list"
 *          },
 *          "post"
 *         },
 *  itemOperations={
 *          "get"={
 *              "security_post_normalize"="is_granted('EDIT_ARTICLE', object)",
 *              "security_message"="only the creator can edit message list"
 *          },
 *         "put",
 *         "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *       },
 * normalizationContext={"groups"={"message:read"}, "swagger_definition_name"="Read"},
 * denormalizationContext={"groups"={"message:write"}, "swagger_definition_name"="Write"},
 * 
 * )
 * @ORM\Entity(repositoryClass=MessagesRepository::class)
 */
class Messages
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"message:read", "message:write"})
     */
    private $subject;

    /**
     * @ORM\Column(type="text")
     * @Groups({"message:read", "message:write"})
     */
    private $message;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"message:read", "message:write"})
     */
    private $isRead;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="messages")
     * 
     */
    private $targetUser;

    public function __construct()
    {
        $this->createAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getIsRead(): ?bool
    {
        return $this->isRead;
    }

    public function setIsRead(bool $isRead): self
    {
        $this->isRead = $isRead;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

      /**
     * @Groups({"message:read"})
     */
    public function getOwner(): ?User
    {
        return $this->targetUser;
    }


    /**
     * @Groups({"message:write"})
     */
    public function setOwner(?User $targetUser): self
    {
        $this->targetUser = $targetUser;

        return $this;
    }
}
