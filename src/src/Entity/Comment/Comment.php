<?php

namespace App\Entity\Comment;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Article\Article;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\User\User;
use App\Repository\CommentRepository;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Doctrine\ORM\Id\UuidGenerator;
/**
 * @ApiResource(
 *  security="is_granted('ROLE_USER')",
 *   collectionOperations={
 *          "get",
 *           "post"
 *         },
 *     itemOperations={
 *          "get",
 *          "put",
 *         "delete",
 *     }, 
 *  *     normalizationContext={"groups"={"comment:read"}, "swagger_definition_name"="Read"},
 *     denormalizationContext={"groups"={"comment:write"}, "swagger_definition_name"="Write"},

 * 
 * )
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 */
class Comment
{
     /**
     * @var \Ramsey\Uuid\UuidInterface
     * @ORM\Column(name="id", type="uuid", length=40, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"comment:read", "comment:write"})
     */

    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     * @Groups({"comment:read", "comment:write"})
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     * @Groups({"comment:read", "comment:write"})
     */
    private $article;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=2,
     *     max=50,
     *     maxMessage="Describe your cheese in 50 chars or less"
     * )
     * @Groups({"comment:read", "comment:write"})
     */
    private $commentary;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    public function __construct(string $id = null)
    {
        $this->id = $id ?? Uuid::uuid4()->toString();
        $this->createAt =  new \DateTimeImmutable();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getCommentary(): ?string
    {
        return $this->commentary;
    }

    public function setCommentary(string $commentary): self
    {
        $this->commentary = $commentary;

        return $this;
    }

    public function getCreateAt(): ?string
    {
        return  Carbon::instance($this->createAt)->diffForHumans();
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
