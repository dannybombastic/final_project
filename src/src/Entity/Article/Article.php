<?php

namespace App\Entity\Article;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Comment\Comment;
use App\Entity\MediaObject\MediaObject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\User;
use App\Entity\Valuation\Valuation;
use App\Entity\Vehicle\Vehicle;
use App\Repository\Article\ArticleRepository;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Doctrine\ORM\Id\UuidGenerator;
use App\Controller\Reorder\ReorderController;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"={
 *                   "denormalization_context"={
 *                                               "groups"={"article:write", "article:item:post"},
 *                                               "security_post_denormalize"="is_granted('ROLE_USER')" 
 *                                              },
 *                   
 *              }
 *         },
 *
 *     itemOperations={
 * 
 *         "put_reorder"={
 *          "shortName"="reorder",
 *         "method"="PUT",
 *         "controller"=ReorderController::class,
 *     },
 *          "get"={
 *              "normalization_context"={"groups"={"article:read", "article:item:get"}},
 *          },
 *          "put"={
 *              "security_post_denormalize"="is_granted('EDIT_ARTICLE', object)",
 *              "security_message"="only the creator can edit article list",
 *               "denormalization_context"={"groups"={"article:write", "article:item:put"}},
 *          },
 *         "delete",
 *         
 *     },
 *     shortName="articles",
 *     normalizationContext={"groups"={"article:read"}, "swagger_definition_name"="Read"},
 *     denormalizationContext={"groups"={"article:write"}, "swagger_definition_name"="Write"},
 *     attributes={
 *          "pagination_items_per_page"=10,
 *          "formats"={"jsonld", "json", "html", "jsonhal", "csv"={"text/csv"}}
 *     }
 * )
 * @ApiFilter(BooleanFilter::class, properties={"isPublished"})
 * @ApiFilter(SearchFilter::class, properties={
 *     "title": "partial",
 *     "description": "partial",
 *     "owner": "exact",
 *     "owner.username": "partial"
 * })
 * 
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 */
class Article
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     * @ORM\Column(name="id", type="uuid", length=40, unique=true)
     * @ORM\Id
     * @ApiProperty(identifier=true)
     * @Groups({"article:write", "article:read", "article:item:post", "user:read"})
     * 
     */

    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"article:read", "article:write", "user:read"})
     * @Assert\NotBlank(message="the title should no be balnk")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * Groups({"article:read", "article:write", "user:read"})
     * @Assert\NotBlank(message="the dscription should no be balnk")
     * 
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"article:read", "article:write", "user:read"})
     */
    private $isPublished = false;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="articles")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"article:read", "article:item:post"})
     * 
     */
    private $owner;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2, nullable=true)
     * @Groups({"article:read", "article:write", "user:read"})
     * @Assert\NotBlank(message="the price should no be balnk")
     * @Assert\Positive
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Vehicle::class, inversedBy="article")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"article:read", "article:write", "user:read"})
     * @Assert\NotBlank(message="the vehicle type should no be balnk")
     */
    private $vehicles;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="article", orphanRemoval=true)
     * 
     */
    private $comments;

 

    /**
     * @ORM\OneToMany(targetEntity=MediaObject::class, mappedBy="article",  cascade={"persist", "remove"})
     * @Groups({"article:item:put", "article:read", "article:write", "user:read"})
     * @ApiProperty(iri="http://schema.org/mediaObjects")
     * @OrderBy({"position" = "ASC"}) 
     */
    private $mediaObjects;

    public function __construct($id = null)
    {
        $this->id = $id ?? Uuid::uuid4()->toString();
        $this->createAt = new \DateTimeImmutable();
        $this->comments = new ArrayCollection();
        $this->mediaObjects = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @Groups({"article:read", "user:read"})
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @Groups({"article:read", "user:read"})
     */
    public function getShortDescription(): ?string
    {
        if (strlen($this->description) < 40) {
            return $this->description;
        }

        return substr($this->description, 0, 40) . '...';
    }

    /**
     * The description of the cheese as raw text.
     *
     * @Groups({"article:write"})
     * @SerializedName("description")
     */
    public function setTextDescription(string $description): self
    {
        $this->description = nl2br($description);

        return $this;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreateAt(): ?string
    {
        return Carbon::instance($this->createAt)->diffForHumans();
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getVehicles(): ?Vehicle
    {
        return $this->vehicles;
    }

    public function setVehicles(?Vehicle $vehicles): self
    {
        $this->vehicles = $vehicles;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setArticle($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getArticle() === $this) {
                $comment->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MediaObject[]
     */
    public function getMediaObjects(): Collection
    {
        return $this->mediaObjects;
    }

    public function addMediaObject(MediaObject $mediaObject): self
    {
        if (!$this->mediaObjects->contains($mediaObject)) {
            $this->mediaObjects[] = $mediaObject;
            $mediaObject->setArticle($this);
        }

        return $this;
    }

    public function removeMediaObject(MediaObject $mediaObject): self
    {
        if ($this->mediaObjects->contains($mediaObject)) {
            $this->mediaObjects->removeElement($mediaObject);
            // set the owning side to null (unless already changed)
            if ($mediaObject->getArticle() === $this) {
                $mediaObject->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
