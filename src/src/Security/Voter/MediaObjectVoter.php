<?php

namespace App\Security\Voter;

use App\Entity\Article\Article;
use App\Entity\MediaObject\MediaObject;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Security;

class MediaObjectVoter extends Voter
{

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }



    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['EDIT_MEDIA'])
            && $subject instanceof MediaObject;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'EDIT_MEDIA':
                // logic to determine if the user can EDIT
                // return true or false
                /** @var MediaObject $subject */
                $article = $subject->getArticle();
                if($article){
                    if ($subject->getArticle()->getOwner() == $user) {
                        return true;
                    }
                }
                // if not article we can modify because there is not owner for this object
                if(!$article){
                    return true;
                }

                
                if ($this->security->isGranted('ROLE_ADMIN')) {

                    return true;
                }
                return false;
        }

        throw new Exception(sprintf('Unhandle attribute %s ', $attribute));
    }
}
