<?php

namespace App\Controller\Article;

use App\Entity\Article\Article;
use App\Repository\Article\ArticleRepository;
use App\Repository\MediaObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;





/**
 * @Route("/article")
 * 
 */
class CreateArticleController extends AbstractController
{


    private SerializerInterface $serializer;
    private ArticleRepository $artilces;

    function __construct(SerializerInterface $serializer, ArticleRepository $artilces)
    {
        $this->serializer = $serializer;
        $this->artilces = $artilces;
    
    }
    /**
     * @Route("/add", name="app_article")
     */
    public function create()
    {
        $this->denyAccessUnlessGranted("ROLE_USER");
        return $this->render('create_article/create.html.twig', [
            'user' => $this->serializer->serialize($this->getUser(), 'jsonld')
        ]);
    }


    /**
     * @Route("/{id}/update", name="app_update_article", methods={"GET","POST"})
     */
    public function update($id)
    {
        $this->denyAccessUnlessGranted("ROLE_USER");
        /** @var Article $article */
        $article = $this->artilces->find($id);

        return $this->render('create_article/create.html.twig', [
            'user' => $this->serializer->serialize($this->getUser(), 'json',  ["user:read"]),
            'article' => $this->serializer->serialize($article, 'json', ["groups" => ["article:read", "media:read"]])

        ]);
    }
}

