<?php

namespace App\Controller\Article;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
 


class UserArticleController extends AbstractController
{

    private SerializerInterface $serializer;
   
    
    function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
    /**
     * @Route("/user/article", name="app_user_article")
     */
    public function index()
    {
        $this->denyAccessUnlessGranted("ROLE_USER");
        return $this->render('user_article/index.html.twig', [
            'user' => $this->serializer->serialize($this->getUser(), 'json', ["groups" => ["article:read", "user:read", "media:item:get"]]),
            'id' => $this->getUser()->getId() ?? 0
        ]);
    }
}
