<?php
// api/src/Controller/CreateBookPublication.php

namespace App\Controller\Reorder;

use App\Entity\Article\Article;
use App\Entity\MediaObject\MediaObject;
use App\Repository\Article\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ReorderController
{

    public function __invoke(ArticleRepository $articleRepo, Article $data, Request $request, EntityManagerInterface $em): Article
    {

        $data_order = json_decode($request->getContent(), true);
        $orderedIds = isset($data_order['data']);
    
        // if is came from api or controller 
        if (!$orderedIds) {
            
            return $data;
        }

        $orderedIds = $data_order['data'];
  
        if (false === $orderedIds && $data_order) {
            throw new BadRequestHttpException("file and id article is required");
        }

        $article = $articleRepo->find($data_order['id']);

        
        $orderedIds = array_flip($orderedIds);
        foreach ($article->getMediaObjects() as $media) {
            /** @var MediaObject $media */
            $media->setPosition($orderedIds[$media->getId()]);
        }

        $em->flush();
        return $article;
    }
}
