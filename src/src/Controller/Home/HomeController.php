<?php

namespace App\Controller\Home;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
 

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(SerializerInterface $serializer)
    {
        return $this->render('home/home.html.twig', [
            'user' => $serializer->serialize($this->getUser(),'jsonld')
        ]);
    }
}
