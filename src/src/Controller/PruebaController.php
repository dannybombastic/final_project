<?php

namespace App\Controller;

use App\Repository\Article\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
 
class PruebaController extends AbstractController
{


    private SerializerInterface $serializer;
    private ArticleRepository $artilces;

    function __construct(SerializerInterface $serializer, ArticleRepository $artilces)
    {
        $this->serializer = $serializer;
        $this->artilces = $artilces;
    }

    /**
     * @Route("/{id}/detail", name="app_detail_article", methods={"GET","POST"})
     */
    public function index($id)
    {
        $article = $this->artilces->find($id);
        return $this->render('prueba/index.html.twig', [
            'user' => $this->serializer->serialize($this->getUser(), 'json'),
            'id' =>  $id
        ]);
    }
}
