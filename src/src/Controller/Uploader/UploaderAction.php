<?php
// api/src/Controller/CreateMediaObjectAction.php

namespace App\Controller\Uploader;

use App\Entity\Article\Article;
use App\Entity\MediaObject\MediaObject;
use App\Repository\Article\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\SerializerInterface;
use Gedmo\Sluggable\Util\Urlizer;

final class UploaderAction  extends AbstractController
{

    public function __invoke(Request $request, ArticleRepository $articleRepository, MediaObject $data, SerializerInterface $serializer): MediaObject
    {

        $uploadedFile = $request->files->get('file');
        $id_article = $request->request->get('article');
       

        // if there is not we are updating the entity
        if (!$uploadedFile && !$id_article) {
 
            return $data;
        }

        $nameFile = $request->files->get('file')->getClientOriginalName();

        $mediaObject = new MediaObject();
        $mediaObject->setArticle($articleRepository->find($id_article));
        $mediaObject->setOriginalFilename(Urlizer::urlize($nameFile));
        $mediaObject->file = $uploadedFile;

        return $mediaObject;
    }
}
