<?php

namespace App\Repository;

use App\Entity\Valuation\Valuation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Valuation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Valuation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Valuation[]    findAll()
 * @method Valuation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ValuationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Valuation::class);
    }

    // /**
    //  * @return Valuation[] Returns an array of Valuation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Valuation
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
