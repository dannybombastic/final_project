import './helper/_helper';
import React, { Component } from 'react';
import ReactDom from 'react-dom';
import '../css/user_article.scss';
import ArticleGrid from './components/articleGrid/articleGrid'


const el = document.getElementById('root_user_article');


// grid for user parts
ReactDom.render(
    <ArticleGrid userid={el.getAttribute('userid')} />,
    el
)

