import React, { Component } from 'react';
import RepLogs from './repLogs';
import PropTypes from 'prop-types';
import { v4 as uuid } from 'uuid';
import store from "../store/storeLogout";
import { saluda } from "../event/actions/actionst";

export default class RepLogApp extends Component {
    constructor(props) {
        super();
        this.store = store;
        this.state = {
            highlightedRowid: null,
            mensage: this.store.sayHi(),
            repLog: [
                { id: uuid(), reps: 25, itemLabel: 'My a Laptop', totalWeightLifted: 112.5 },
                { id: uuid(), reps: 10, itemLabel: 'Big Fat Cat', totalWeightLifted: 180 },
                { id: uuid(), reps: 4, itemLabel: 'Big Fat Cat', totalWeightLifted: 72 }
            ],

            numberOfHearts: 1
        };
     
        this.handlerClickRow = this.handlerClickRow.bind(this);
        this.handlerItemSubmit = this.handlerItemSubmit.bind(this);
        this.handleHeartChange = this.handleHeartChange.bind(this);
        this.handelDeleteItem = this.handelDeleteItem.bind(this);
    }

    handlerClickRow(repLog) {
        this.setState({ highlightedRowid: repLog.id });
    }

    handlerItemSubmit(itemName, reps) {

        const newRepLog = {

            id: uuid(),
            reps: reps,
            itemLabel: itemName,
            totalWeightLifted: 152.14

        }

        this.setState(prevState => {

            // were avoiding mutate the state 
            const newRep = [...prevState.repLog, newRepLog];
            return { repLog: newRep };

        });
    }

    handleHeartChange(heartCount) {
        this.setState({
            numberOfHearts: heartCount
        });
    }


    handelDeleteItem(itemId) {
        console.log(itemId);
        // remove the repo log without mutating state
        this.setState((prevState) => {
            return { repLog: prevState.repLog.filter((repLog) => repLog.id !== itemId) };
        });


    }


    componentDidMount(){
        this.store.addHiListener(this.evento);
        if (this.store.sayHi().length === 0) saluda();
    }
  
    componentWillUnmount() {
        this.store.removeHiListener(this.evento);
    }


    evento() {

       console.log("evento emitido");
    }


    

    render() {

        return (
            <div>
                <div>{ this.state.mensage.map((msg) => msg )}</div>
                <RepLogs
                    {...this.props}
                    {...this.state}
                    onRowClick={this.handlerClickRow}
                    onNewItemSubmit={this.handlerItemSubmit}
                    handleHeartChange={this.handleHeartChange}
                    onDeleteItem={this.handelDeleteItem}
                />
            </div>


        )

    }
}





RepLogApp.propTypes = {

    with_heart: PropTypes.bool,
    repLog: PropTypes.array,
    handleHeartChange: PropTypes.func,
    onDeleteItem: PropTypes.func



}