import React, { Component } from 'react';
import PropTypes from 'prop-types';



export default class RepLogCreator extends Component {

    constructor(props) {
        super(props);

        this.state = {
            quantityError: ''
        };

        this.quantityInput = React.createRef();
        this.itemSelect = React.createRef();

        this.handlerFormSubmit = this.handlerFormSubmit.bind(this);
    }

    handlerFormSubmit(event) {
        event.preventDefault();
        const { onNewItemSubmit } = this.props;
        const itemSelect = this.itemSelect.current;
        const quantityInput = this.quantityInput.current;

        if (quantityInput.value <= 0) {
            this.setState({
                quantityError: 'introduce a positive number pls'
            });

            return;
        }


        onNewItemSubmit(
            itemSelect.options[itemSelect.selectedIndex].text,
            quantityInput.value
        );

        itemSelect.selectedIndex = 0;
        quantityInput.value = '';
        this.setState({
            quantityError: ''
        });

    }

    render() {

        const { quantityError } = this.state;

        return (
            <div>
                <form method="POST" onSubmit={this.handlerFormSubmit} role="form">

                    <div className={`form-group ${quantityError ? 'has-error' : ''}`}>
                        <label htmlFor="text">label</label>
                        <input type="number" className="form-control" ref={this.quantityInput} name="reps" id="text" placeholder="Input field" />
                        {quantityError && <span className="help-block">{quantityError}</span>}
                    </div>
                   

                        <select
                            ref={this.itemSelect}
                            id="inputselect"
                            className="form-control"
                            required="required">
                            <option value="big fat cat">big fat cat</option>
                            <option value="big fat ass">big fat ass</option>
                            <option value="big fat woman">big fat woman</option>
                        </select>
 
                    <button type="submit" className="btn-button btn-primary">Submit</button>
                </form>
            </div>
        )
    }

}

RepLogCreator.propTypes = {

    onNewItemSubmit: PropTypes.func.isRequired,

};