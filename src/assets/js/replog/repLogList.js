import React from 'react';
import PropTypes from 'prop-types';


export default function ReploList(props){
                               
    /**
     * repLog data from state
     * highlightedRowid state attribute
     * onRowClick parent fuction
     * 
     */
    const { 
        highlightedRowid, 
        onRowClick, 
        repLog, 
        onDeleteItem
     } = props;
                                        
     const handelDeleteItemClick = (event, replogId) => {
           event.preventDefault();
           
            onDeleteItem(replogId);
     }
    return (
        <tbody>
            {repLog.map((repLog) => (
                <tr
                    key={repLog.id}   // react likes have an id per item collection
                    className={highlightedRowid === repLog.id ? 'bg-info' : ''}
                    onClick={() => onRowClick(repLog)}
                >

                    <td>{repLog.itemLabel}</td>
                    <td>{repLog.reps}</td>
                    <td>{repLog.totalWeightLifted}</td>
                    <td><span onClick={ (event) => handelDeleteItemClick(event,repLog.id)}><i className="fa fa-times"></i></span></td>
                </tr>
            ))}
        </tbody>
    );
}



ReploList.propTypes = {

    highlightedRowid: PropTypes.any,
    onRowClick: PropTypes.func.isRequired,
    repLog: PropTypes.array.isRequired

};