import React, { Component } from 'react';
import RepLoList from './repLogList';
import PropTypes, { func } from 'prop-types';
import RepLogCreator from './repLogCreator';
//import RepLogCreator from './repLogCreatorControllerComponent';






const calculate = repLogs => repLogs.reduce((total, log) => total + log.totalWeightLifted, 0);

export default function RepLogs(props) {
    // destructurin to lazy load

    const {
        with_heart,
        highlightedRowid,
        onRowClick,
        repLog,
        onNewItemSubmit,
        numberOfHearts,
        handleHeartChange,
        onDeleteItem
    } = props;

    let heart = [];
    if (with_heart) {

        heart = [...Array(numberOfHearts)].map((e, i) => <span key={i}>o</span>);
    }




    return (
        <div className="row">
            <div className="col-12">

                <input
                    type="range"
                    value={numberOfHearts}
                    onChange={(e) => {
                        handleHeartChange(+e.target.value);
                    }}
                />
                <div className="row">
                    {heart}
                </div>
                <div className="table-responsive">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>What</th>
                                <th>How many times?</th>
                                <th>Weight</th>
                                <th>trash</th>
                            </tr>
                        </thead>
                        <RepLoList
                            highlightedRowid={highlightedRowid}
                            onRowClick={onRowClick}
                            repLog={repLog}
                            onDeleteItem={onDeleteItem}
                        />
                        <tfoot>
                            <tr>
                                <td>
                                    {calculate(repLog)}
                                </td>
                            </tr>

                        </tfoot>
                    </table>

                </div>



                <RepLogCreator
                    onNewItemSubmit={onNewItemSubmit}

                />


            </div>
        </div>
    );

}

RepLogs.propTypes = {
    with_heart: PropTypes.bool,
    highlightedRowid: PropTypes.any,
    onRowClick: PropTypes.func,
    repLog: PropTypes.array.isRequired,
    onNewItemSubmit: PropTypes.func,
    numberOfHearts: PropTypes.number.isRequired,
    handleHeartChange: PropTypes.func
};