import React, { Component } from 'react';
import PropTypes from 'prop-types';



export default class RepLogCreator extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedIndex: '',
            quantityValue: 0,
            quantityError: ''
        };



        this.handlerFormSubmit = this.handlerFormSubmit.bind(this);
        this.handelSelectItemChange = this.handelSelectItemChange.bind(this);
        this.handelQuantityChange = this.handelQuantityChange.bind(this);
    }

    handlerFormSubmit(event) {
        event.preventDefault();
        const { onNewItemSubmit } = this.props;
        const { selectedIndex , quantityValue } = this.state;

        if (quantityValue <= 0) {
            this.setState({
                quantityError: 'introduce a positive number pls'
            });

            return;
        }

        onNewItemSubmit(
            'TOdO',
            quantityValue
        );
        
        this.setState({
            selectedIndex: '',
            quantityInput: 0,
            quantityError: ''
        });

    }

    handelSelectItemChange(event) {

        this.setState({
            selectedIndex: event.target.value
        });

    }

    handelQuantityChange(event) {

        this.setState({
            quantityValue: event.target.value
        });

    }


    render() {

        const { quantityError, quantityValue, selectedItemId } = this.state;

        return (
            <div>
                <form method="POST" onSubmit={this.handlerFormSubmit} role="form">

                    <div className={`form-group ${quantityError ? 'has-error' : ''}`}>
                        <label htmlFor="text">label</label>
                        <input type="number"
                            value={quantityValue}
                            className="form-control"
                            name="reps" id="text"
                            step="1"
                            placeholder="Input field"
                            onChange={this.handelQuantityChange}
                        />
                        {quantityError && <span className="help-block">{quantityError}</span>}
                    </div>
                    <div className="form-group">

                        <select
                            value={selectedItemId}
                            id="inputselect"
                            className="form-control"
                            onChange={this.handelSelectItemChange}
                            required="required"
                        >
                            <option value="big fat cat">big fat cat</option>
                            <option value="big fat ass">big fat ass</option>
                            <option value="big fat woman">big fat woman</option>
                        </select>

                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        )
    }

}

RepLogCreator.propTypes = {

    onNewItemSubmit: PropTypes.func.isRequired,

};