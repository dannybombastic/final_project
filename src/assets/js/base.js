import './helper/_helper';
import "../css/base.scss";
import React from "react";
import ReactDom from 'react-dom';
import UserForm from "./components/userForm/userForm";


hideSidebar();
createLoginComponent();

function hideSidebar() {
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
}

function createLoginComponent() {
    const el = document.getElementById('root_login');
    

    // Login and user registration
    ReactDom.render(
        <UserForm user={el.getAttribute('data-params')} route={el.getAttribute('route')} />,
        el
    );
}