import './helper/_helper';
import React, { Component } from 'react';
import ReactDom from 'react-dom';
import '../css/article.scss';
import Article from "./components/articleForm/articleComponent";


const el = document.getElementById('root_article');

// atricle create and update
ReactDom.render(
    <Article update={el.getAttribute('update')} article={el.getAttribute('article')} user={el.getAttribute('user')}/>,
    el
);

