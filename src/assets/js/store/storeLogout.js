import { EventEmitter } from "events";
import dispatcher from "../event/events";
import actionTypes from "../event/actions/actionType";

import dispacher from "../event/events";

const CHANGE_EVENT = "change";

const HI_EVENT = "hinge";

let logout = [];
let hi = [];

class StoreLogout extends EventEmitter {
    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addHiListener(callback) {
        this.on(HI_EVENT, callback);
    }

    removeHiListener(callback) {
        this.removeListener(HI_EVENT, callback);
    }

    emitHi() {
        this.emit(HI_EVENT);
    }

    sayHi() {
        return hi;
    }
    getLogout() {
        return logout;
    }
}


const store = new StoreLogout();

dispatcher.register((action) => {
    switch (action.actionTypes) {
        case actionTypes.GET_LOGOUT:
            logout = action.logout;
            store.emitChange();
            break;
        case actionTypes.HI_EVENT:
            hi = action.hi;
            store.emitChange();
            break;
        default:
    }
});

export default store;