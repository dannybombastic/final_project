import '../css/detail.scss';
import './helper/_helper.js';
import React from 'react';
import ReactDom from 'react-dom';
import { render } from 'react-dom';
import Carousel from './components/carrousel/carrousel';

const el = document.getElementById('detail_article');

ReactDom.render(<Carousel article={el.getAttribute('article')} />
    ,
    el);