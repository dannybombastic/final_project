import 'abortcontroller-polyfill/dist/polyfill-patch-fetch';
import { v4 as uuid } from 'uuid';



export const URL_BASE = 'http://localhost';
export const URL_BASE_LOGOUT = 'http://localhost/user/logout';
export const DELETE_ARTICLE_MEDIAOBJECT = '/api/media_objects/';
export const VEHICLES = '/api/vehicles';
export const EDIT_MEDIANAME = '/api/media_objects/';
export const ORDER_ARTICLE_MEDIA = '/api/articles/';
export const POST_ARTICLE = '/api/articles';
export const GET_ARTICLE_NO_PAGINATION = '/api/articles?page=1&pagination=false&isPublished=true';
export const PUT_ARTICLE = '/api/articles/';
export const DLETE_USER_ARTICLE = '/api/articles/';
export const GET_USER_ARTICLES = '/api/users/';
export const GET_ARTICLE_BY_ID = '/api/articles/';

export default class Fetch extends Object {
    constructor() {
        super();

    }

    loginUser(email, password, controller = new AbortController().signal) {
        let data = JSON.stringify({ email: email, password: password });
        return fetch(URL_BASE + '/user/login', {
            signal: controller,
            credentials: 'same-origin',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: data

        }).then(res => {
            if (res.status === 204) {
                return res.headers.get('location');
            }
            if (res.status === 401) {
                return res
            }
            return "";
        });

    }

    getUserData(url, controller = new AbortController().signal) {

        return fetch(URL_BASE + url, {
            signal: controller,
            credentials: 'same-origin',
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(res => {
            console.log("api-res", res);
            return res.json();
        });
    }
 
    getUserLogout(controller = new AbortController().signal) {

        return fetch(URL_BASE_LOGOUT, {
            credentials: 'same-origin',
            signal: controller,

            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(res => {
            console.log("api-res", res);
            return res;
        });
    }



    registerUser(username, email, password, phonenumber = 'empty', controller = new AbortController().signal) {
        let data = JSON.stringify({
            "@id": uuid(),
            "email": email,
            "password": password,
            "username": username,
            "phoneNumber": phonenumber
        });
        console.log("data", data);
        return fetch(URL_BASE + '/api/users', {
            signal: controller,
            credentials: 'same-origin',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: data

        }).then(res => {

            return res.json()
        });

    }


    // article Form
    getData(url, controller = new AbortController().signal) {


        return fetch(URL_BASE + url, {
            signal: controller,
            credentials: 'same-origin',
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then(res => {

            return res.json()
        });

    }

    deleteArticle(id, controller = new AbortController().signal) {


        return fetch(URL_BASE + DELETE_ARTICLE_MEDIAOBJECT + id, {
            signal: controller,
            credentials: 'same-origin',
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then(res => {

            return res
        });

    }

    getVehicle(controller = new AbortController().signal) {


        return fetch(URL_BASE + VEHICLES, {
            signal: controller,
            credentials: 'same-origin',
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then(res => {

            return res.json()
        });

    }

    editMediaObjectname(id_media, data, controller = new AbortController().signal) {


        return fetch(URL_BASE + EDIT_MEDIANAME + id_media, {
            signal: controller,
            credentials: 'same-origin',
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)

        }).then(res => {

            return res.json()
        });

    }

    editOrderMediaByArticle(id_article, data, controller = new AbortController().signal) {


        return fetch(URL_BASE + ORDER_ARTICLE_MEDIA + id_article, {
            signal: controller,
            credentials: 'same-origin',
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: data

        }).then(res => {

            return res.json()
        });

    }


    registerArticle(art_uuid, title, owner, description, price, vehicles, isActive, controller = new AbortController().signal) {

        let data = {
            "id": art_uuid,
            "title": title,
            "owner": owner,
            "price": price,
            "description": description,
            "vehicles": vehicles,
            "isPublished": isActive == 'on' ? true : false
        };

        console.log("data send", data);
        data = JSON.stringify(data);

        return fetch(URL_BASE + POST_ARTICLE, {
            signal: controller,
            credentials: 'same-origin',
            method: 'POST',
            headers: new Headers({
                'Content-type': 'application/ld+json'
            }),
            body: data

        }).then(res => {
            return res.json();

        });
    }


    updateArticle(art_uuid, title, description, price, vehicles, isActive, controller = new AbortController().signal) {

        let data = {
            "title": title,
            "price": price,
            "description": description,
            "vehicles": vehicles,
            "isPublished": isActive == 'on' ? true : false
        };


        data = JSON.stringify(data);

        return fetch(URL_BASE + PUT_ARTICLE + art_uuid, {
            signal: controller,
            credentials: 'same-origin',
            method: 'PUT',
            headers: new Headers({
                'Content-type': 'application/ld+json'
            }),
            body: data

        }).then(res => {
            return res.json();

        });
    }




    getArticlesByUser(user_id, controller = new AbortController().signal) {


        return fetch(URL_BASE + GET_USER_ARTICLES + user_id, {
            signal: controller,
            credentials: 'same-origin',
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then(res => {

            return res.json()
        });

    }


    deleteUserArticle(id_article, controller = new AbortController().signal) {


        return fetch(URL_BASE + DLETE_USER_ARTICLE + id_article, {
            signal: controller,
            credentials: 'same-origin',
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(res => {

            return res
        });

    }


    getArticles(controller = new AbortController().signal) {


        return fetch(URL_BASE + GET_ARTICLE_NO_PAGINATION, {
            signal: controller,
            credentials: 'same-origin',
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then(res => {

            return res.json()
        });

    }

    async getArticlesById(id, controller = new AbortController().signal) {


        return fetch(URL_BASE + GET_ARTICLE_BY_ID + id, {
            signal: controller,
            credentials: 'same-origin',
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
    
        }).then(res => {
    
            return  res.json()
        });
    
    }
    
}




