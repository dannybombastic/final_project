import React, { useRef, useState, useEffect } from "react";
import '../../helper/_helper';
import add from '../../../static/img/add_circle.svg';
import article_list from '../../../static/img/article_list.svg';
import logout from '../../../static/img/exit_to_app.svg';

export default function dropDawn(props) {

    const [user, setUser] = useState(props.user_info === "" ? "" : props.user_info);
    const { onUserLogOut } = props;

    return (
        <div>
            <ul className="list-unstyled">
                <li>
                    <a href="#home" data-toggle="collapse" aria-expanded="true"
                        className="dropdown-toggle text-bold btn btn-outline-light btn-block">{user.username}</a>
                    <ul className="collapse list-unstyled show text-bold" id="home">

                        <li>
                            <div className="d-flex justify-content-start" data-toggle="tooltip" data-placement="left" title="Get prueba">

                                <a className={props.routes === 'app_detail_article' ? 'btn-block p-2 active' : 'btn-block p-2'} href="/prueba">
                                    prueba
                                          </a>
                                <a href="/prueba">

                                    <img className="d-flex p-2" src={add} />
                                </a>
                            </div>
                        </li>
                        <li>
                            <div className="d-flex justify-content-start" data-toggle="tooltip" data-placement="left" title="Watch your parts list">

                                <a className={props.routes === 'app_user_article' ? 'btn-block p-2' : 'btn-block p-2'} href="/user/article">
                                    My parts
                                          </a>
                                <a href="/user/article">
                                    <img className="d-flex p-2" src={article_list} />
                                </a>
                            </div>
                        </li>
                        <li>
                            <div className="d-flex justify-content-start" data-toggle="tooltip" data-placement="left" title="Add a new part">

                                <a className={props.routes === 'app_article' ? 'btn-block p-2 active' : 'btn-block p-2'} href="/article/add">
                                    Add Article
                                          </a>
                                <a href="/article/add">

                                    <img className="d-flex p-2" src={add} />
                                </a>
                            </div>
                        </li>
                        <li>
                            <div className="d-flex flex-row" data-toggle="tooltip" data-placement="left" title="Get log out">
                                <a className="btn-block p-2" onClick={onUserLogOut}>
                                    Logout</a>
                                <a onClick={onUserLogOut}>
                                    <img className="p-2" src={logout} />
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    );

}