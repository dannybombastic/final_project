import React, { useRef, useState, useEffect } from "react";
import "../../helper/_helper";
import Fetch from "../../fetch/api";



export default function _form(props) {
    const [vehicles, setVehicles] = useState([]);
    const [isError, setIsError] = useState(false);
    const [message, setMessage] = useState([]);
    const [itemSeleted, setItemSeleted] = useState(false);

    const controller = new AbortController();
    const signal = controller.signal;
    const custom_fetch = new Fetch();
    const isUpdate = JSON.parse(props.update).true;

    // ref
    const select_vehicle = useRef(null);

    const article_entity = isUpdate ? JSON.parse(props.article) : ''
    const user_id = props.user.id;
    const article_uuid = props.uuid;



    useEffect(() => {

        isUpdate ? setItemSeleted(article_entity.vehicles.id) : true;
        addVehicles();
        return () => {

        };
    }, [props]);

    const addVehicles = () => {
        custom_fetch.getVehicle(signal).then((data) => {
            if(data){
                setVehicles([...data]);
            }
            
        });
    };


    const handleChange = (event) => {
        console.log(event.target.value);
        setItemSeleted(event.target.value);
    };


    const handelOnSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData(e.target);
        console.log("item", formData.get('title'));
        console.log("uuid", article_uuid);
        custom_fetch.registerArticle(
            article_uuid,
            formData.get('title'),
            '/api/users/' + user_id,
            formData.get('description'),
            formData.get('price'),
            '/api/vehicles/' + itemSeleted,
            formData.get('isActive')
        ).then((data) => {
            console.log(data);
            if (data.violations) {
                if (data.violations.length > 0) {
                    setIsError(true);
                    setMessage(data.violations);
                    console.log("error", message);
                    return;
                }
            }
            const { onSubmit } = props;
            onSubmit();
            console.log("info", data);
            setIsError(false);
        }).catch((value) => {
            setIsError(true);
            setMessage([{ message: 'invalid type of vehicle' }]);
            console.log("caTH", value);
        });
    }


    const handelOnSubmitUpdate = (e) => {
        e.preventDefault();
        const formData = new FormData(e.target);
        console.log("item", formData.get('title'));
        console.log("uuid", article_uuid);
        custom_fetch.updateArticle(
            article_uuid,
            formData.get('title'),
            formData.get('description'),
            formData.get('price'),
            '/api/vehicles/' + itemSeleted,
            formData.get('isActive')
        ).then((data) => {
            console.log(data);
            if (data.violations) {
                if (data.violations.length > 0) {
                    setIsError(true);
                    setMessage(data.violations);
                    console.log("error", message);
                    return;
                }
            }
            console.log("info", data);
            setIsError(false);
        });
    }


    const closeAlert = () => {
        setIsError(false);
    };

    return (
        <div>
            {isError &&
                <div className="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>{message.map((msg, index) => <div key={index}>{msg.message}</div>)}</strong>
                    <button type="button" onClick={closeAlert} className="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

            }

            {isUpdate &&
                <div>
                    <form method="POST" onSubmit={(event) => handelOnSubmitUpdate(event)}>

                        <div className="form-group is-focused mt-5">
                            <label htmlFor="title" className="bmd-label-floating"><h6>Title:</h6></label>
                            <input type="text" defaultValue={article_entity.title} className="form-control" placeholder="" name="title" id="title" />
                        </div>

                        <div className="form-group is-focused">
                            <label htmlFor="description" className="bmd-label-floating"><h6>Description</h6> </label>
                            <input type="text" defaultValue={article_entity.shortDescription} className="form-control" placeholder="Description" name="description" id="description" />
                            <span className="help"><small>We need a description explanation about your part.</small></span>
                        </div>
                        <div className="form-group is-focused">
                            <label htmlFor="pwd" className="bmd-label-floating">Price:</label>
                            <input type="number" defaultValue={article_entity.price} className="form-control" placeholder="Price" id="price" name="price" />
                            <span className="help"><small>We need to know about the price.</small></span>

                        </div>
                        <div className="form-group is-focused">
                            <label htmlFor="inputselect" className="bmd-label-floating"><h6>Select Type:</h6> </label>
                            <select
                                ref={select_vehicle}
                                id="inputselect"
                                className="form-control"
                                required="required"
                                onChange={handleChange}
                                value={itemSeleted}
                            >
                                {vehicles.map(vehicle => <option key={vehicle.id} value={vehicle.id}>{vehicle.type} </option>)}
                            </select>
                        </div>
                        <div className="form-group form-check is-focused">
                            <label className="bmd-label-floating">
                                <input className="form-check-input" name="isActive" defaultChecked={article_entity.isPublished} type="checkbox" placeholder="is active" /><h6>isActive:</h6>
                            </label>
                        </div>
                        <button type="submit" className="btn btn-primary btn-block text-center">Submit</button>
                    </form>

                    <button type="button" className="btn btn-large btn-block btn-danger bg-danger text-center text-white">delete</button>
                </div>
            }
            {!isUpdate &&
                <form method="POST" onSubmit={handelOnSubmit}>
                    <div className="form-group mt-5 is-focused">
                        <label htmlFor="title" className="bmd-label-floating"><h6>Title:</h6></label>
                        <input type="text" maxLength="254" className="form-control" placeholder="Title" name="title" id="title" required />
                        <span className="help"><small>We need a litle explanation about your part. </small></span>
                    </div>

                    <div className="form-group is-focused">
                        <label htmlFor="description" className="bmd-label-floating"><h6>Description:</h6></label>
                        <input type="text" maxLength="254" className="form-control" placeholder="" name="description" id="description" required />
                        <span className="help"><small>We need a description explanation about your part.</small></span>
                    </div>
                    <div className="form-group is-focused">
                        <label htmlFor="pwd" className="bmd-label-floating"><h6>Price:</h6></label>
                        <input type="number" className="form-control" placeholder="Price" id="price" name="price" required />
                        <span className="help"><small>We need to know about the price.</small></span>

                    </div>
                    <div className="form-group is-focused">
                        <label htmlFor="inputselect" className="bmd-label-floating"><h6>Select Type:</h6> </label>
                        <select
                            ref={select_vehicle}
                            id="inputselect"
                            className="form-control"
                            onChange={handleChange}
                            value={itemSeleted}
                            required="required">
                            {vehicles.map(vehicle => <option key={vehicle.id} value={vehicle.id}>{vehicle.type} </option>)}
                        </select>
                    </div>
                    <div className="form-group form-check is-focused">
                        <label className="form-check-label" className="bmd-label-floating">
                            <input className="form-check-input" name="isActive" type="checkbox" /> <h6>isActive:</h6>
                        </label>
                    </div>
                    <button type="submit" className="btn btn-primary btn-block text-center">Submit</button>
                </form>

            }

            <hr />



        </div>
    );
}