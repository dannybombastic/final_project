import React, { useRef, useState, useEffect } from "react";
import "../../helper/_helper";
import { validateEmail, validatePassword, validatePhone, validateUsername } from "../../validator/validate";
import Dropzone from 'dropzone';
import 'dropzone/dist/dropzone.css';
import Sortable from 'sortablejs';
import '@fortawesome/fontawesome-free/js/all.js';
import '@fortawesome/fontawesome-free/css/all.css';
import { v4 as uuid } from 'uuid';
import Fetch from "../../fetch/api";
import Form from "./_form";


Dropzone.autoDiscover = false;
export default function articleFrom(props) {
    const { article, user } = props;
    const uuid_update = props.isUpdate ? JSON.parse(article).id : false;
    const [vehicles, setVehicles] = useState([]);
    const [message, setMessage] = useState('initialState');
    const [uuuid, setUuuid] = useState(uuid_update ? uuid_update : uuid());
    const [media, setMedia] = props.article === "" ? useState([]) : useState(JSON.parse(props.article).mediaObjects);
    const element_ul = useRef(null);
    const file_name = useRef(null);


    const controller = new AbortController();
    const signal = controller.signal;
    const custom_fetch = new Fetch()
    var sortable = null;



    useEffect(() => {


        getMediaObjects();
        props.isUpdate ? initiateDropzone() : null;
        initiateSotrable();

        return () => {
            controller.abort();
        };
    }, [props]);

    const handelSubmit = () => {
        initiateDropzone();
    };


    const handleReferenceEdit = (event, id_li) => {

        const reference = media.find(reference => {

            return reference.id == id_li;
        });

        reference.originalFilename = event.currentTarget.value;
        delete reference['position'];
        console.log("reference send", reference);
        custom_fetch.editMediaObjectname(id_li, reference, signal).then((response) => {
            console.log("succes", response);

        });

    }

    const getMediaObjects = () => {
        custom_fetch.getData(element_ul.current.getAttribute('data-url'), signal).then((data) => {
            console.log("data data", data);
            let isArray = Array.isArray(data);
            if (isArray) {
                addMedia(data);
            }
        });
    };


    const addMedia = (media_data) => {

        setMedia([...media_data]);

    };

    const pushMedia = (media_data) => {

        setMedia(oldArray => [...oldArray, media_data]);

    };

    const initiateDropzone = () => {
        var formElement = document.querySelector('.js-reference-dropzone');
        if (!formElement) {
            return;
        }

        var dropzone = new Dropzone(formElement, {
            paramName: 'file',
            init: function (file, done) {
                this.on('success', function (file, data) {

                    pushMedia(data);
                    console.log("data dropzone", media);
                });
                this.on('error', function (file, data) {
                    if (data.detail) {
                        this.emit('error', file, data.detail);
                    }

                });
            },

        });

    };



    const initiateSotrable = () => {

        const elements = element_ul.current;
        sortable = Sortable.create(elements, {
            handle: '.drag-handle',
            animation: 150,
            onEnd: () => {

                custom_fetch.editOrderMediaByArticle(
                    uuuid,
                    JSON.stringify({ data: sortable.toArray(), id: uuuid }),
                    signal
                ).then((response) => {
                    console.log("sortable", response);
                });
            },
            onUpdate: function (evt) {
                console.log("update", evt);
            },
        });

    };

    const handleReferencedelete = (event) => {
        const li = event.currentTarget.closest('.list-group-item');
        const id = li.getAttribute('data-id');


        custom_fetch.deleteArticle(id, signal).then((data) => {
            console.log("delete response", data);
            setMedia(media.filter(reference => reference.id !== id));
            if (data.status == 204) {
                li.classList.add('d-none');
            }

        });

    }

    return (
        <div>
            <div className="row justify-content-center">
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 pr-5 border-right">
                    <Form
                        {...props}
                        uuid={uuuid}
                        onSubmit={handelSubmit}
                    />
                </div>
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center">
                    <h1>Upload files</h1>
                    <div className="row justify-content-center">
                        <div className="col-xs-12 col-sm-12 w-100 col-md-12 col-lg-12">
                            <form className="dropzone js-reference-dropzone"
                                action={`/api/media_objects`} method="POST"
                                encType="multipart/form-data">
                                <input type="hidden" name="article" id="article" defaultValue={uuuid} />
                            </form>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul className="list-group overflow-auto js-reference-list" style={{ maxHeight: '400px', overflowY: 'auto' }} ref={element_ul}
                            data-url={`/api/media_objects?article=${uuuid}`}>
                            {media.map((image) =>
                                <li key={image.id} className="list-group-item" data-id={image.id}>
                                    <div className="d-flex flex-column w-100">
                                        <div className="p-2">
                                            <input type="text" ref={file_name} className="form-control js-edit-filename" onBlur={(event) => handleReferenceEdit(event, image.id)} id={"input-update-" + image.id}
                                                defaultValue={image.originalFilename} />
                                        </div>
                                        <div className="d-sm-inline-flex flex-xl-rowr">

                                            <div className="">
                                                <img src={image.contentUrl} height="50" />
                                            </div>
                                            <div className="d-flex flex-xl-row-reverse w-100">
                                                <div className="">
                                                    <button type="button" className="btn"><span className="drag-handle fa fa-sort"></span></button>
                                                </div>
                                                <div className="">
                                                    <a className="btn btn-link btn-sm text-dark" href={image.contentUrl}>
                                                        <i className="fa fa-download" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div>
                                                    <button className="js-reference-delete btn" onClick={(event) => handleReferencedelete(event)} > <i className="fa fa-trash" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>)}
                        </ul>
                    </div>
                </div>
            </div>
        </div>);
}



