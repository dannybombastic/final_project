import React, { Component, useRef, useState, useEffect } from "react";
import Fetch from "../../fetch/api";
import "../../helper/_helper";
import store from "../../store/storeLogout";
import ArticleFrom from "./articleForm";


export default function Article(props) {
    const { update, user } = props;
    const [msg, setmsg] = useState('hola');
    const [state, setState] = useState({
        isUpdate: JSON.parse(update).true ? true : false,
        user: user === "" ? "" : JSON.parse(user)
    });

    const owner = user !== "" ? JSON.parse(user) : { username: 'default' };


    useEffect(() => {
        store.addChangeListener(cleanDataUser);
        return () => {
            store.removeChangeListener(cleanDataUser);
        };
    }, [props]);

    const cleanDataUser = () => {

        window.location.href = "/";

    };

    return (
        <div>
            {state.isUpdate &&
                <div className="container-fluid">
                    <div>{owner && <div> {owner.username} </div>}</div>
                    <div className="">
                        <h1>Update your piece</h1>
                        <hr />
                        <ArticleFrom
                            {...props}
                            {...state}

                        />
                        <hr />
                    </div>
                </div>
            }
            {!state.isUpdate &&
                <div className="container-fluid">

                    <div className="row justify-content-center">
                        <div>{owner && <div><h1 className="text-capitalize"> {owner.username}</h1> </div>}</div>
                    </div>

                    <div className="">
                        <h1>Add new piece</h1>
                        <hr />
                        <ArticleFrom
                            {...props}
                            {...state}
                        />
                        <hr />
                    </div>
                </div>
            }

        </div>
    );




}