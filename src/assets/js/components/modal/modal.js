import React, { Component, Children } from 'react';
import '../../../css/componets/modal.scss';
import PropTypes from 'prop-types';
import Fetch from '../../fetch/api';
 




export default class Modal extends Component {

    constructor(props) {
        super(props);
        //class we use to make request to the backend
        this.fecth = new Fetch();
        // user info we pass it through the root tag

        // initial state
        this.state = {
            arial: true,
        };
        this.show = this.show.bind(this);

    }

    show() {
        const { onVisible } = this.props;

        onVisible();
        

    }

    render() {


        const { isModalVisible } = this.props;
        return (
            <div>
               
                { isModalVisible &&
                    <div
                        className="modal fade show"
                        id="modalLoginForm"
                        tabIndex="-1"
                        role="dialog"
                        aria-labelledby="myModalLabel"
                        style={{ display: 'block', paddingRight: '17px' }}
                        aria-modal={isModalVisible}>
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header text-center">
                                    <h4 className="modal-title w-100 font-weight-bold">Sign in</h4>
                                    <button type="button" onClick={this.show} className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div className="modal-body mx-3">
                                    <div>{this.props.children}</div>
                                </div>

                            </div>
                        </div>
                    </div>

                }
            </div>







        );
    }



}

