import React, { useState, useEffect } from "react";
import { render } from 'react-dom';
import Fetch from '../../fetch/api';
import { Carousel } from 'react-responsive-carousel';
import delete_svg from "../../../static/img/delete-24px.svg";
import edit_svg from "../../../static/img/edit-24px.svg";
import "react-responsive-carousel/lib/styles/carousel.min.css";



export default function home(props) {

    const [data, setData] = useState([]);
    // api 
    const controller = new AbortController();
    const signal = controller.signal;
    const custom_fecth = new Fetch();

    useEffect(() => {
        custom_fecth.getArticles(signal).then((data) => {
            console.log("data", data);
            setData(data);
        });
        return () => {

        };
    }, [props]);


    return (<div>
        <div className="container-fluid">
            <div className="row">
                {data.length && data.map((article) =>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 p-2" key={article.id} >
                        <div className="card">
                            <div className="card-body p-0">
                                <Carousel
                                    showThumbs={true}
                                    swipeable={true}
                                    axis="vertical"
                                >
                                    {article.mediaObjects.map((media, index) =>
                                        <div key={index}>
                                            <img id={media.id} src={media.contentUrl} className="rounded-lg img-responsive" alt="Image" />
                                        </div>
                                    )}
                                </Carousel>
                                <div className="card-body">
                                    <p style={{ fontSize: '1rem' }} className="card-title text-bold">{article.title}</p>
                                    <h6 className="card-subtitle mb-2 text-muted">{article.vehicles.type}</h6>
                                    <p className="card-text">{article.shortDescription}</p>
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col">
                                                <div className="d-flex justify-content-start">

                                                    <a href={`/${article.id}/detail`} className="btn btn-block text-center text-nowrap">
                                                        check it out
                                                                </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </div>
    </div>);
}