import React, { useRef, useState, useEffect } from "react";
import RegisterComponent from "../registerForm/registerComponent";
import Login from "../LoginForm/LoginComponent";
import 'abortcontroller-polyfill/dist/polyfill-patch-fetch';

export default function userForm(props) {
    const [isLoged, setIsLoged] = useState(props.user === "" ? "" : true);

    const handelLoged = (value) => {
        setIsLoged(value);
    };
    return (
        <div>
            <Login
                {...props}
                onLogin={handelLoged}
            />
            {!isLoged &&
                <RegisterComponent
                    {...props}
                />}
        </div>
    );
}