import React, { useRef, useState, useEffect } from "react";
import Fetch from '../../fetch/api';
import store from "../../store/storeLogout";
import '../../helper/_helper';
import delete_svg from "../../../static/img/delete-24px.svg";
import edit_svg from "../../../static/img/edit-24px.svg";
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";


export default function articleGrid(props) {
    const { userid } = props;
    const [articles, setData] = useState([]);

    // api 
    const controller = new AbortController();
    const signal = controller.signal;
    const custom_fecth = new Fetch();


    useEffect(() => {
        console.log("proba", JSON.parse(userid).id);
        custom_fecth.getArticlesByUser(JSON.parse(userid).id, signal).then((data) => {
            console.log("user", data.articles);
            setData(data.articles);
        });
        store.addChangeListener(cleanDataUser);

        return () => {
            store.removeChangeListener(cleanDataUser);
        };
    }, [props]);


    const cleanDataUser = () => {

        window.location.href = "/";

    };
    const handelDelete = (id) => {
        custom_fecth.deleteUserArticle(id, signal).then((data) => {
            console.log("delete", data);
        });
    };

    const getData = (data) => {
        console.log(data)
        return data;
    };

    return (

        <div className="container-fluid">
            <div className="row">
                {articles && articles.map((article) =>

                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 p-2" key={article.id}>
                       <div className="card" >
                            <div className="card-body p-0">
                                <Carousel
                                    showThumbs={false}
                                    swipeable={true}
                                >
                                    {article.mediaObjects.map((media, index) =>
                                        <div key={index}>
                                            <img id={media.id} src={media.contentUrl} className="rounded-lg" height="200" alt="Image" />
                                        </div>
                                    )}
                                </Carousel>
                                <div className="card-body">
                                    <p style={{ fontSize: '1rem' }} className="card-title text-bold">{article.title}</p>
                                    <h6 className="card-subtitle mb-2 text-muted">{article.vehicles.type}</h6>
                                    <p className="card-text">{article.shortDescription}</p>
                                    <div className="col">
                                        <div className="d-flex justify-content-start">
                                            <a href="#" onClick={(event) => handelDelete(article.id)} className="btn text-center btn-block card-link">
                                                <img src={delete_svg} className="img-responsive" alt="Image" />
                                            </a>
                                            <a href={`/article/${article.id}/update`} className="btn btn-block text-center text-wrap card-link">
                                                <img src={edit_svg} className="img-responsive" alt="Image" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                )}

            </div>

        </div >

    );
}