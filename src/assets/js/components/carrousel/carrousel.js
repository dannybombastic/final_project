import React, { useRef, useState, useEffect } from "react";
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Fetch from "../../fetch/api";

export default function carrousel(props) {
    const [article_id, setArticleId] = useState(props.article);
    const [article_user, setArticle] = useState({});
    const [owner, setOwner] = useState({});

    const controller = new AbortController();
    const signal = controller.signal;
    const custom_fetch = new Fetch()


    useEffect(() => {
        custom_fetch.getArticlesById(article_id, signal).then((data) => {
            setArticle(data);
        });
        return () => {

        };
    }, [props]);

    return (
        <div>
            {article_user.owner && <div className="container-fluid">
                <div className="row text-left">
                    <a className="btn btn-success btn-lg" href="/">Back to menu</a>
                </div>
                <div className="row p-5">
                    <div className="row align-items-center">
                        <div className="col-sm">
                            <div className="col-6">
                                <h1> Description </h1>

                                <p className="d-inline"> {article_user.description}</p>
               
                            </div>
                            <div className="col-6">

                                <h1 className="pt-5"> Phone number </h1>

                                <p className="d-inline"> {article_user.owner.phoneNumber}</p>
                            </div>
                        </div>
                    </div>

                    <div className="col-sm">
                        <Carousel
                            showThumbs={true}
                            swipeable={true}
                            autoPlay={true}
                            infiniteLoop
                            axis="horizontal">
                            {
                                article_user.mediaObjects && article_user.mediaObjects.map((media, index) =>
                                    <div key={index}>
                                        <img id={media.id} src={media.contentUrl} className="rounded-lg" alt="Image" />
                                        <p className="legend">{article_user.shortDescription}</p>
                                    </div>)
                            }
                        </Carousel>

                    </div>
                </div>

            </div>}

        </div>

    )

}