import React, { useRef, useState, useEffect } from "react";
import Fetch from '../../fetch/api';
import '../../helper/_helper';
import { validateEmail } from "../../validator/validate";



export default function loginForm(props) {

    const [isError, setIsError] = useState(false);
    const [errorMessage, setMessage] = useState('holy');

    const password_ref = useRef(null);
    const email_ref = useRef(null);

    // api 
    const controller = new AbortController();
    const signal = controller.signal;
    const custom_fecth = new Fetch();

    var message_delete_timeout = 0;

    useEffect(() => {
        validating();
        return () => {
            controller.abort();
            console.log("aborted", controller.signal.aborted);
            email_ref.current = null;
            password_ref.current = null;
            clearTimeout(message_delete_timeout);
        };
    }, []);

    const deleteMessageError = (msg) => {
        setMessage(msg);
        clearTimeout(message_delete_timeout);
        message_delete_timeout = setTimeout(() => {
            setIsError(false);
            controller.abort();
            message_delete_timeout = 0;
        }, 3000);

    }

    const onSubmitForm = (event) => {
        // onUserinfo send the data to prent and onFlag change the login state        
        const { onUserInfo, onCloseMdodal } = props;

        event.preventDefault();

        let email = email_ref.current.value;
        let password = password_ref.current.value;

        if (!validateEmail(email)) {
            setIsError(true);
            deleteMessageError("Enter a valid email");
            return;
        }

        // TODO use validator here
        const data = custom_fecth.loginUser(email, password, signal).then(data => {
            console.log("data in", data);
            if (data.status === 401) {
                setIsError(true);
                deleteMessageError(data.statusText);
                return;
            }
            // sending data user from sign in from
            custom_fecth.getUserData(data, signal).then(user_data => {
                console.log(data);
                if (data.violations) {
                    if (data.violations.length > 0) {
                        setIsError(true);
                        deleteMessageError(data.detail);
                        return;
                    }
                }
                setIsError(false);
                onUserInfo(user_data);
                onCloseMdodal();

            });

        });



    };

    // validate fields from forms

    const validating = () => {

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                event.preventDefault();

                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');

            }, false);
        });
    }


    return (
        <div>
            {isError && <div>
                <div className="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>{errorMessage}!</strong> You should check in on some of those fields below.
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>}

            <form method="POST" onSubmit={(evento) => onSubmitForm(event)} className="needs-validation" acceptCharset="UTF-8" noValidate >
                {/* <span>{isError && <div><p className=" text-center text-danger"> {} </p> </div>}</span> */}
                <div className="mb-5 is-focused">

                    <label data-error="wrong" data-success="right" htmlFor="email" className="bmd-label-floating"> <i className="fas fa-envelope prefix grey-text"></i>Your email</label>
                    <input key="email" ref={email_ref} type="email" id="email" name="email" className="form-control" required />
                    <div className="invalid-feedback">
                        Please provide a valid email.
                                         </div>
                    <div className="valid-feedback">
                        Looks good.
                </div>
                </div>
                <div className="mb-4 is-focused">

                    <label data-error="wrong" data-success="right" htmlFor="password" className="bmd-label-floating"> <i className="fas fa-lock prefix grey-text"></i> Your password</label>
                    <input key="password" ref={password_ref} type="password" id="password" name="password" className="form-control" required />
                    <div className="invalid-feedback">
                        Please provide a pasword.
                </div>
                    <div className="valid-feedback">
                        There you go.
                </div>
                </div>
                <div className="modal-footer d-flex justify-content-center">
                    <button type="submit" id="submit" className="btn btn-default">Login</button>
                </div>
            </form >
        </div>
    );



}