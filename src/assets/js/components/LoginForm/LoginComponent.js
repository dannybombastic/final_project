import '../../helper/_helper';
import '../../../css/base.scss';
import React, { Component } from 'react';
import Modal from "../modal/modal";
import Loginform from "./loginForm";
import Fetch from '../../fetch/api';
import login from '../../../static/img/login.svg';
import DropDown from '../dropDown/dropDown';
import store from "../../store/storeLogout";


export default class Login extends Component {
    constructor(props) {
        super(props);
        const { user, route } = this.props;
        console.log("user", user === "");
        this.state = {
            user_info: user === "" ? false : JSON.parse(user),
            isLoged: user === "" ? false : true,
            isModalVisible: false,
            routes: route

        };
 
        this.handelUserInfo = this.handelUserInfo.bind(this);
        this.onUserLogOut = this.onUserLogOut.bind(this);
        this.handelModal = this.handelModal.bind(this);
        const controller = new AbortController();
        this.signal = controller.signal;
        this.custom_fetch = new Fetch();
    }

    //get info from the form 
    handelUserInfo(user_inf) {

        if (user_inf) {
            this.setState({
                user_info: user_inf,
                isLoged: true
            });

            const { onLogin } = this.props;
            // set isLoged in parent to true to make regiter button disapear
            onLogin(true);
        }
    }

    componentWillUnmount(){
        controller.abort();
    }

    
    // make user log out
    onUserLogOut() {
        const { onLogin } = this.props;
        // set isLoged in parent to true to make regiter button disapear
  
        this.custom_fetch.getUserLogout(this.signal).then(() => {
            onLogin(false);
          
            this.setState({
                user_info: '',
                isLoged: false,
                isModalVisible: false,
            });
           store.emitChange();
          // store.emitHi();

        });
     
    }

    // tell the modal when to be showed
    handelModal() {

        this.setState({
            isModalVisible: !this.state.isModalVisible
        });
    }

    render() {

        return (
            <div className="text-left">
                {this.state.isLoged && 
                <DropDown
                {...this.props}
                {...this.state}
                onUserLogOut={this.onUserLogOut}
                />
                   
                }
                {!this.state.isLoged && <div>
                    <div className="d-flex justify-content-start register-button">
                        <a type="button" onClick={this.handelModal} className="text-bold btn-block p-2">Login
                       
                        </a>
                        <img className="p-2" src={login} />
                        
                    </div>
                </div>}
                <Modal
                    {...this.state}
                    {...this.props}
                    onVisible={this.handelModal}
                >
                    <Loginform
                        key="1"
                        {...this.props}
                        {...this.state}
                        onCloseMdodal={this.handelModal}
                        onUserInfo={this.handelUserInfo}

                    />
                </Modal>

            </div>
        );
    }
}

