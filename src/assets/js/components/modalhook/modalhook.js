import React, { useRef, useState, useEffect } from "react";
import Fetch from '../../fetch/api';
import 'abortcontroller-polyfill/dist/polyfill-patch-fetch';



export default function modalhook(props) {

    const [state, setState] = useState(props);
    const show = () => {
        const { onVisible } = props;
        onVisible();

    };
 
    useEffect(() => {
        setState(props);
        return () => {

        };
    }, [props]);

    return (
        <div>
            {state.isModalVisible &&
                <div
                    className="modal fade show"
                    id="modalLoginForm"
                    tabIndex="-1"
                    role="dialog"
                    aria-labelledby="myModalLabel"
                    style={{ display: 'block', paddingRight: '17px' }}
                    aria-modal={props.isModalVisible}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header text-center">
                                <h4 className="modal-title w-100 font-weight-bold">Sign in</h4>
                                <button type="button" onClick={show} className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div className="modal-body mx-3">
                                <div>{props.children}</div>
                            </div>

                        </div>
                    </div>
                </div>
            }

        </div>


    );

}