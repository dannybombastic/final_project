import React, { useRef, useState, useEffect } from "react";
import Fetch from '../../fetch/api';
import 'abortcontroller-polyfill/dist/polyfill-patch-fetch';
import ModalHook from '../modalhook/modalhook';
import RegisterForm from './registerForm'
import login from '../../../static/img/login.svg';


export default function registerComponent(props) {
    const { user } = props;
    console.log("user", user);
    const [state, setState] = useState({
        isModalVisible: false
    });

    const handelModal = () => {
        setState({
            isModalVisible: !state.isModalVisible,
        });
    };
    const { isLoged } = props;
    return (
        <div>
            <div >
                {!isLoged && <div className="d-flex justify-content-start register-button"> <a type="button" onClick={handelModal} className="p-2  btn-block">Register</a> <img className="p-2" src={login} /></div>}
                
            </div>
            <ModalHook
                {...state}
                onVisible={handelModal}
            >
                <RegisterForm 
                onClose={handelModal}
                />
            </ModalHook>
        </div >
    );
}