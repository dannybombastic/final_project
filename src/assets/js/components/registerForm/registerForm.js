import React, { useRef, useState, useEffect } from "react";
import "../../helper/_helper";
import Fetch from '../../fetch/api';
import { validateEmail, validatePassword, validatePhone, validateUsername } from "../../validator/validate";



export default function RegisterForm(props) {
    const [isError, setIsError] = useState(false);
    const [messgae, setMessage] = useState('no error');
    
    const controller = new AbortController();
    const signal = controller.signal;

    const username = useRef(null);
    const email = useRef(null);
    const phonenumber = useRef(null);
    const password = useRef(null);
    const password_2 = useRef(null);
    const custom_fecth = new Fetch();
    var message_delete_timeout = 0;

    useEffect(() => {
   
        return () => {
            controller.abort();
            clearTimeout(message_delete_timeout);
        };
    }, [props]);


    const onSubmitFrom = () => {
        const { onClose } = props;
        if (!validateEmail(email.current.value)) {
            setIsError(true);
            deleteMessageError("Enter a valid email");
            return;
        }

        if (typeof validatePassword(password.current.value, password_2.current.value) === 'string') {
            setIsError(true);
            deleteMessageError(validatePassword(password.current.value, password_2.current.value));
            return;
        }
        if (typeof validatePhone(phonenumber.current.value) === 'string') {
            
            setIsError(true);
            deleteMessageError(validatePhone(phonenumber.current.value));
            return;
        }
        if (typeof validateUsername(username.current.value) === 'string') {
            setIsError(true);
            deleteMessageError(validateUsername(username.current.value));
            return;
        }
        setIsError(false);

        custom_fecth.registerUser(
            username.current.value,
            email.current.value,
            password.current.value,
            phonenumber.current.value,
            signal).then((data) => {

                if (data.violations) {
                    if (data.violations.length > 0) {
                        setIsError(true);
                        deleteMessageError(data.detail);
                    }
                }
                setIsError(true);
                deleteMessageError(data.detail);
              
                onClose();
             
            });
    };

    const deleteMessageError = (msg) => {
        setMessage(msg);
        clearTimeout(message_delete_timeout);
        message_delete_timeout = setTimeout(() => {
            setIsError(false);
            message_delete_timeout = 0;
        }, 2000);

    }

    return (
        <div>
            {
                isError &&
                <div>
                    <div className="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>{messgae}!</strong> You should check in on some of those fields below.
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            }
            <form method="POST" className="form" className="needs-validation" acceptCharset="UTF-8" noValidate>
                {/*  username */}
                <div className="col">
                    <label htmlFor="username" className="col-sm-12 col-form-label">User name</label>
                    <div className="col-sm-12">
                        <input type="text" className="form-control-plaintext" ref={username} placeholder="Jhon Doe" id="username" required />
                    </div>
                    <div className="invalid-feedback">
                        Please provide a valid username
                                         </div>
                    <div className="valid-feedback">
                        Looks good.
                </div>
                </div>
                {/*  email */}
                <div className="col">
                    <label htmlFor="email" className="col-sm-2 col-form-label">Email</label>
                    <div className="col-sm-12">
                        <input type="email" className="form-control" id="email" ref={email} placeholder="example@example.exa" required />
                    </div>
                    <div className="invalid-feedback">
                        Please provide a valid email.
                                         </div>
                    <div className="valid-feedback">
                        Looks good.
                </div>
                </div>
                {/*  phone */}
                <div className="col p-2">
                    <label htmlFor="phonenumber" className="col-sm-12 col-form-label">Phone number <small> (optional)</small></label>
                    <div className="col-sm-12">
                        <input type="text" className="form-control-plaintext" ref={phonenumber} placeholder="+34606197854" id="phonenumber" />
                    </div>
                </div>
                {/*  pasword */}
                <div className="row">
                    <div className="col">
                        <label htmlFor="password" className="col-sm-2 col-form-label">Password</label>
                        <input type="password" className="form-control" ref={password} id="password" placeholder="password" required />
                    </div>
                    <div className="col">
                        <label htmlFor="password_2" className="col-sm-2 col-form-label">Password</label>
                        <input type="password" className="form-control" ref={password_2} id="password_2" placeholder="Confirm your password" required />
                    </div>
                </div>


            </form>
            <div className="row justify-content-center p-3">
                <div className="col-xs">
                    <button type="button" onClick={onSubmitFrom} className="btn btn-primary">Send it</button>
                </div>
            </div>

        </div>
    );
}