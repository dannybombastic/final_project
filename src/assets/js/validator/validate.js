
export function validateEmail(email) {

    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
        return true;
    }

    return false;
}


export function validateLength(chain) {

    if (chain.length > 255) {
        error = "Text too long";
        return error;
         
    }

    return true;
}

export function validateNumber(number) {

    if (isNaN(x) || x < 1 || x > 10) {
        error = "Number not valid";
       return error;
      }
    return true;       
}

export function validateUsername(username) {
    console.log("name");
    var error = "";
    var illegalChars = /\W/; // allow letters, numbers, and underscores

    if (username == "") {
        error = "You didn't enter a username.\n";
        return error;

    }
    if ((username.length < 5) || (username.length > 15)) {
        error = "The username is the wrong length.\n";
        return error;

    }
    if (illegalChars.test(username)) {
        error = "The username contains illegal characters.\n";
        return error;

    }
    return true;
}

export function validatePhone(phonenumber) {
    console.log("phone");
    var error = "";
    var stripped = phonenumber.replace(/[\(\)\.\-\ ]/g, '');

    if (phonenumber == "") {
        error = "You didn't enter a phone number.\n";
        return error;
    }
    if (isNaN(parseInt(stripped))) {
        error = "The phone number contains illegal characters. Don't include dash (-)\n";
        return error;
    }
    if (!(stripped.length == 9)) {
        error = "The phone number is the wrong length. Make sure you included an area code. Don't include dash (-)\n";
        return error;
    }
    return true;
}

export function validatePassword(pass_one, pass_two) {
    let password1 = pass_one;
    let password2 = pass_two;

    // If password not entered 
    if (password1 == '') {
        let error = "Please enter Password";
        return error;
    }
    // If confirm password not entered 
    if (password2 == '') {
        let error = "Please enter confirm password";
        return error;
    }
    // If Not same return False.     
    if (password1 !== password2) {
        let error = "Password did not match: Please try again...";
        return error;
    }
    return true;

} 