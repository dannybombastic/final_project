/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../css/home.scss';
import './helper/_helper.js';
import React from 'react';
import { render } from 'react-dom';
import RepLogAppo from './replog/repLogApp';
import Home from "./components/home/home";


render(
    <Home/>,
    document.getElementById('root')
)

// const shouldShowText = true;

// render(
//     <div>
//         <RepLogAppo with_heart={shouldShowText} />
//     </div>,
//     document.getElementById('root')

// );

//  $("form#data").submit(function(e) {
//      console.log("pasando")
//     e.preventDefault();    
//     var formData = new FormData(this);
   
//     $.ajax({
//         url: '/api/media_objects',
//         type: 'POST',
//         data: formData,
//         success: function (data) {
//             console.log("succes", data);
//         },
//         error: function (data){
//             console.log("error", data);
//         },
//         cache: false,
//         contentType: false,
//         processData: false
//     });
// });

// console.log('Hello Webpack Encore! Edit me in assets/js/app.js');
