import dispatcher from "../events";
import actionTypes from "./actionType";
import Fetch from "../../fetch/api";


export function getLogout() {
    const custom_fetch = new Fetch();
    dispatcher.dispatch({
        actionTypes: actionTypes.GET_LOGOUT,
        logout: custom_fetch.getUserLogout(),
    });
}



export function saluda() {
    const custom_fetch = new Fetch();
    dispatcher.dispatch({
        actionTypes: actionTypes.HI_EVENT,
        hi: ['hola cabroness'],
    });
}


